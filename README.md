# Read before the Download!
**Warning**! This repository is **private** and won't work for others than me unless you have the **exact** same configuration as me in your worlds. I won't share it with others for now since it **may be used for illegal purposes**, since it uses a third party plugin which allows access to content you don't have the rights to access. I can legally use it since i **own the original content** on books, so there is no difference for me between copying the informations from the book or importing and translating them manually.

# Please retain from downloading this module! It won't work for you!
