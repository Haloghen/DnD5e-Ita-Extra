Hooks.on('init', () => {

    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'FoundryVTT-dnd5extra-it',
            lang: 'it',
            dir: 'compendium'
        });
    }
});
